import { Egress } from './../../models/egress';
import { Component, OnInit, Input } from '@angular/core';
import { EgressService } from 'src/app/services/egress.service';

@Component({
  selector: 'app-egress',
  templateUrl: './egress.component.html',
  styleUrls: ['./egress.component.css']
})
export class EgressComponent implements OnInit {
  
  egresos: Egress[] = [];
  @Input() ingresoTotal: number;
  constructor(private eS: EgressService) {
  }

  ngOnInit(): void {
    this.egresos = this.eS.egresos;
  }

  eliminarEgreso(egreso: Egress){
    this.eS.eliminarIngresoService(egreso);
  }

  calcularPorcentaje(egreso: Egress){
  return egreso.valor / this.ingresoTotal; 
  }

}
