import { Component, OnInit } from '@angular/core';
import { Entrance } from 'src/app/models/entrance';
import { EntranceService } from 'src/app/services/entrance.service';

@Component({
  selector: 'app-entrance',
  templateUrl: './entrance.component.html',
  styleUrls: ['./entrance.component.css']
})
export class EntranceComponent implements OnInit {
  
  ingresos: Entrance[] = [];
  constructor(private iS: EntranceService) { }

  ngOnInit(): void {
    this.ingresos = this.iS.ingresos;
  }

  eliminarIngreso(ingreso: Entrance){
    this.iS.eliminarIngresoService(ingreso);
  }

}
