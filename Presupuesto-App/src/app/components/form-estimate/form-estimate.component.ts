import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { EgressService } from 'src/app/services/egress.service';
import { EntranceService } from 'src/app/services/entrance.service';
import { Entrance } from 'src/app/models/entrance';

@Component({
  selector: 'app-form-estimate',
  templateUrl: './form-estimate.component.html',
  styleUrls: ['./form-estimate.component.css']
})
export class FormEstimateComponent implements OnInit {
  descripcion: string;
  valor: number;
  tipo = 'ing';

  constructor(private eS: EgressService,
              private iS: EntranceService) { }

  ngOnInit(): void {
  }

  tipoOperacion(event){
    return this.tipo = event.target.value;
  }

  agregarValor(f: NgForm){
    if(!f.valid){
      console.log('Invalido');
      return;
    }
    if(this.tipo == 'ing'){
      this.iS.ingresos.push(new Entrance(this.descripcion, this.valor));
      this.iS.guardarStorage();
      console.log('ingresos');
    }else{
      this.eS.egresos.push(new Entrance(this.descripcion, this.valor));
      this.eS.guardarStorage();
      console.log('egresos');
    }
  }

}
