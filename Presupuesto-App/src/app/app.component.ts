import { Component } from '@angular/core';
import { EntranceService } from './services/entrance.service';
import { EgressService } from './services/egress.service';
import { Entrance } from './models/entrance';
import { Egress } from './models/egress';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Presupuesto-App';
  ingresos: Entrance[] = [];
  egresos: Egress[] = [];
  constructor(private egressS: EgressService,
              private entranceS: EntranceService) {
              this.egressS.cargarStorage();
              this.entranceS.cargarStorage();
              this.ingresos = entranceS.ingresos;
              this.egresos = egressS.egresos;
  }

  getPresupuesTotal(){
    return this.getTotalEntrance() - this.getTotalEgress();
  }

  getTotalEgress() {
    let sumaTotal: number = 0;
    this.egresos.forEach(element => {
      sumaTotal += element.valor;
    });

    return sumaTotal;
  }

  getTotalEntrance() {
    let sumaTotal: number = 0;
    this.ingresos.forEach(element => {
      sumaTotal += element.valor;
    });

    return sumaTotal;
  }

  getTotalPercentaje() {
    return this.getTotalEgress() / this.getTotalEntrance();
  }
}
