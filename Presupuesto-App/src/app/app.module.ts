import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { EntranceComponent } from './components/entrance/entrance.component';
import { EgressComponent } from './components/egress/egress.component';
import { FormEstimateComponent } from './components/form-estimate/form-estimate.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EntranceComponent,
    EgressComponent,
    FormEstimateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
