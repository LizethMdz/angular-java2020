import { Entrance } from './../models/entrance';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EntranceService {

  constructor() { }

  ingresos: Entrance[] = [
  ];


  eliminarIngresoService(ingreso: Entrance){
    const indice:  number = this.ingresos.indexOf(ingreso);
    this.ingresos.splice(indice, 1); 
    this.guardarStorage();
  }

  guardarStorage(){
    localStorage.setItem('ing', JSON.stringify(this.ingresos))
  }

  cargarStorage(){
    if (localStorage.getItem('egr') && localStorage.getItem('ing')) {
      this.ingresos = JSON.parse(localStorage.getItem('ing'));
    } else {
      this.ingresos = [];
    }
  }
}
