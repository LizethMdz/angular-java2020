import { Egress } from './../models/egress';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EgressService {

  constructor() { }

  egresos: Egress[] = [
  ];

  eliminarIngresoService(egreso: Egress){
    const indice:  number = this.egresos.indexOf(egreso);
    this.egresos.splice(indice, 1); 
    this.guardarStorage();
  }

  guardarStorage(){
    localStorage.setItem('egr', JSON.stringify(this.egresos));
  }

  cargarStorage(){
    if (localStorage.getItem('egr') && localStorage.getItem('ing')) {
      this.egresos = JSON.parse(localStorage.getItem('egr'));
    } else {
      this.egresos = [];
    }
  }
}
