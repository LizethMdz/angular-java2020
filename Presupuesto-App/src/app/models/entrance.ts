export class Entrance {
    descripcion: string;
    valor: number;

    constructor(descripcion: string, valor: number){
        this.descripcion = descripcion;
        this.valor = valor;
    }
}
