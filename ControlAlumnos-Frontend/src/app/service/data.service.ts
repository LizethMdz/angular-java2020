import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Persona } from '../model/persona.model';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {}

  urlBase = "http://localhost:8080/ControlAlumnos_Backend/webservice/personas";

  obtenerPersonas(){
    return this.httpClient.get(this.urlBase);
  }

  agregarPersona(persona: Persona){
    return this.httpClient.post(this.urlBase, persona);
  }

  modificarPersona(id: number, persona: Persona){
    let url = this.urlBase + '/' + id;
    return this.httpClient.put(url, persona).subscribe(
      (resultado) => {
        console.log('Resultado de la persona modificada ', resultado)
      },
      (error) => console.log('Error al modificar ', error)
      
    );
  }

  deletePersona(id: number){
    let url = this.urlBase + '/' + id;
    return this.httpClient.delete(url).subscribe(
      (resultado) => {
        console.log('Resultado de la persona eliminada ', resultado)
      },
      (error) => console.log('Error al eliminar ', error)
      
    );
  }
}
