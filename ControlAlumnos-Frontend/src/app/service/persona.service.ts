import { Injectable } from '@angular/core';
import { Persona } from '../model/persona.model';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  personas: Persona[] = [];

  constructor(private dataService: DataService) {}

  //Se usa para modificar el valor del arreglo debido a la llamada asincrona
  setPersonas(personas: Persona[]){
    this.personas = personas;
  }

  obtenerPersonas(){
    return this.dataService.obtenerPersonas();
  }

  agregarPersona(persona: Persona){
    console.log('Persona Agregada: ' + persona.nombre);
    this.dataService.agregarPersona(persona).subscribe(
      (persona: Persona) =>{
        console.log('Se agrega al arreglo la persona insertada: ' + persona.idPersona);
        this.personas.push(persona);
      }
    );
  }

  encontrarPersona(idPersona: number){
    const persona: Persona = this.personas.find(persona => persona.idPersona == idPersona);
    console.log('Persona encontrada: ' + persona.idPersona + ' ' + persona.nombre);
    return persona;
  }

  modificarPersona(id: number, persona: Persona){
    console.log('Modificar Persona: ' + persona.idPersona);
    const personaModLocal = this.personas.find(persona  => persona.idPersona == id);
    personaModLocal.idPersona = persona.idPersona;
    personaModLocal.nombre = persona.nombre;
    personaModLocal.apellido = persona.apellido;
    personaModLocal.email = persona.email;
    personaModLocal.telefono = persona.telefono;
    this.dataService.modificarPersona(id, persona);
  }

  eliminarPersona(id: number){
    console.log('Eliminar Persona con id: ' + id);
    const index = this.personas.findIndex(persona => persona.idPersona == id);
    this.personas.splice(index, 1);
    this.dataService.deletePersona(index);
  }
}
