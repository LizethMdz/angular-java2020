import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
// NOTE Importación del HTTP
import { HttpClientModule } from '@angular/common/http';
//Importacion de FormsModule
import { FormsModule } from '@angular/forms';
//IMportacion de los componentes
import { AppComponent } from './app.component';
import { PersonasComponent } from './components/personas/personas.component';
import { FormularioComponent } from './components/formulario/formulario.component';

// NOTE Importacion del servicio
import { DataService } from './service/data.service';
import { PersonaService } from './service/persona.service';



@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DataService,
    PersonaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
