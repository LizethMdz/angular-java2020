import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonasComponent } from './components/personas/personas.component';
import { FormularioComponent } from './components/formulario/formulario.component';


const routes: Routes = [
  { path: 'personas', component: PersonasComponent, children:[
      { path: 'agregar', component: FormularioComponent },
      { path: ':idPersona', component: FormularioComponent },
  ] },
  { path: '**', pathMatch: 'full', redirectTo: 'personas' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
