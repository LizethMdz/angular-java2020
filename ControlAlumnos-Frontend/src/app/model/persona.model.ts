
export class Persona {
    idPersona: number;
    nombre: string;
    apellido: string;
    email: string;
    telefono: string;
    constructor(idPersona: number, nombre: string, apellido: string, email: string, telefono:string){
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
    }
}