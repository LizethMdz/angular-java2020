import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PersonaService } from 'src/app/service/persona.service';
import { Persona } from 'src/app/model/persona.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  idPersona: number;
  nombreInput: string;
  apellidoInput: string;
  emailInput: string;
  telefonoInput: string;

  formvalido: boolean;

  constructor(private personaService : PersonaService,
              private router: Router,
              private route: ActivatedRoute) {
              }

  ngOnInit(): void {
    this.formvalido =  true;
    this.idPersona = this.route.snapshot.params['idPersona'];
    
    console.log('Id recuperado: ' + this.idPersona);
    if(this.idPersona != null){
      const persona = this.personaService.encontrarPersona(this.idPersona);
      if (persona != null) {
        this.idPersona = persona.idPersona;
        this.nombreInput = persona.nombre;
        this.apellidoInput = persona.apellido;
        this.emailInput = persona.email;
        this.telefonoInput = persona.telefono;
      }
    }
  }

  onGuardarPersona(form: NgForm){
    if ( form.invalid ) {
      console.log('Formulario no válido');
      this.formvalido =  false;
      return;
    }else{
      const personaGuardar = new Persona(this.idPersona, this.nombreInput, this.apellidoInput, 
        this.emailInput, this.telefonoInput);
      if(this.idPersona != null){
        this.personaService.modificarPersona(this.idPersona, personaGuardar);
        
      }else{
        this.personaService.agregarPersona(personaGuardar);
      }
      this.formvalido =  true;
      this.router.navigate(['personas']);
    }

  }

  onEliminar(id:number){
    if(id != null){
      console.log('Persona a eliminar por ID: ' + id);
      this.personaService.eliminarPersona(id);
    }
    this.router.navigate(['personas']);
  }

}
