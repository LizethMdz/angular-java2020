import { Component, OnInit } from '@angular/core';
import { PersonaService } from 'src/app/service/persona.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Persona } from 'src/app/model/persona.model';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  personas: Persona[];
  constructor(private personaService: PersonaService,
              private router: Router,
              private route: ActivatedRoute) { 

                
              }

  ngOnInit() {
    this.personaService.obtenerPersonas().subscribe(
      (resultado: Persona[]) => {
        this.personas = resultado;
        this.personaService.setPersonas(this.personas);
        console.log('Personas obtenidas: ' + this.personas);
    });
  }

  irAgregar(){
    this.router.navigate(['./personas/agregar']);
  }

}
