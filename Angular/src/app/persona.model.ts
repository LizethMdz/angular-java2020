export class PersonaModel {
    nombre: string;
    edad: number;

    constructor(nombre: string, edad: number){
        this.edad = edad;
        this.nombre = nombre;
    }
}
