import { LoginService } from './services/login.service';
import { LoggingService } from './services/logging.service';
import { DataService } from './services/data.service';
import { LoginGuardService } from './services/login-guard.service';
import { ContenedorComponent } from './components/contenedor.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersonasComponent } from './components/personas/personas.component';
import { FormpersonasComponent } from './components/formpersonas/formpersonas.component';
import { CalculadoraComponent } from './components/calculadora/calculadora.component';
import { CalculadoraResultadoComponent } from './components/calculadora-resultado/calculadora-resultado.component';
import { ErrorComponent } from './components/error/error.component';
import { LoginComponent } from './components/login/login.component';
import { PersonasService } from './services/personas.service';


@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    FormpersonasComponent,
    CalculadoraComponent,
    CalculadoraResultadoComponent,
    ContenedorComponent,
    ErrorComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [LoginGuardService, DataService, LoggingService, LoginService, PersonasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
