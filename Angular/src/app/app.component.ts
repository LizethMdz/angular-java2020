import { Component, NgModule, OnInit } from '@angular/core';
///Importar en FIREBASE
import * as firebase from 'firebase';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Angular Miselanea';
  resPadre : number;
  verResPadre: boolean;
  

  constructor(private loginS: LoginService){}

  // onResultado(resultado: number){
  //   this.resPadre = resultado;
  // }

  // onPersonaAdd(persona: PersonaModel){
  //   this.logService.enviarMensajesConsola("Persona agregada: " + persona.nombre);
  //   this.personaService.agregarPersona(persona);
  // }

  // onVerResultado(verRes: boolean){
  //   this.verResPadre = verRes;
  //   console.log('Monitoreando');
  //   console.log(this.verResPadre);
  // }

  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyAjPU4mNdgKD9z_POXLA3dkDDeTxNMwlVI",
      authDomain: "personal-angular-1e27d.firebaseapp.com",
    });
  }

  esAutenticado(){
    return this.loginS.isAuthenticated();
  }
  
  salir(){
    this.loginS.logout();
  }
}
