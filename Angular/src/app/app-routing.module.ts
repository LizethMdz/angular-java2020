import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormpersonasComponent } from './components/formpersonas/formpersonas.component';
import { ContenedorComponent } from './components/contenedor.component';
import { ErrorComponent } from './components/error/error.component';
import { LoginComponent } from './components/login/login.component';
import { LoginGuardService } from './services/login-guard.service';


const routes: Routes = [
  { path: '', component: ContenedorComponent, canActivate:[LoginGuardService] },
  { path: 'personas', component: ContenedorComponent, canActivate:[LoginGuardService] , children: [
    { path: 'agregar', component: FormpersonasComponent },
    { path: ':id', component: FormpersonasComponent }
  ]},
  { path: 'login', component: LoginComponent },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
