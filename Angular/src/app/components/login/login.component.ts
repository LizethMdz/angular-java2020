import { LoginService } from './../../services/login.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginS: LoginService) { }

  ngOnInit(): void {
  }

  login(f: NgForm){
    const email = f.value.email;
    const password = f.value.password;
    this.loginS.login(email, password);
  }

}
