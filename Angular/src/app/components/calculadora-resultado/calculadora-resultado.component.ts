import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-calculadora-resultado',
  templateUrl: './calculadora-resultado.component.html',
  styleUrls: ['./calculadora-resultado.component.css']
})
export class CalculadoraResultadoComponent implements OnInit {
  
  @Input() result:number;
  @Input() verResult:boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
