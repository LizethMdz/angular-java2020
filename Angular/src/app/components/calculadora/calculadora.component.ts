import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  one: number;
  second: number;

  @Output() resultadoHijo= new EventEmitter<number>();

  @Output() verResultadoHijo = new EventEmitter<boolean>();

  verAlert = false;


  sumar(f: NgForm){
    if(f.invalid && !this.verAlert){
      this.verAlert = true;
      return;
    }
    this.verAlert = false;
    let res = true;
    this.verResultadoHijo.emit(res);
    let  resFinal = this.one + this.second;
    this.resultadoHijo.emit(resFinal);
  }

  restar(f: NgForm){
    this.verAlert;
    if(f.invalid && !this.verAlert){
      this.verAlert = true;
      return;

    }
    this.verAlert = false;
    let res = true;
    this.verResultadoHijo.emit(res);
    let  resFinal = this.one - this.second;
    this.resultadoHijo.emit(resFinal);
  }

  multiplicar(f: NgForm){
    if(f.invalid && !this.verAlert){
      this.verAlert = true;
      return;

    }
    this.verAlert = false;
    let res = true;
    this.verResultadoHijo.emit(res);
    let  resFinal = this.one * this.second;
    this.resultadoHijo.emit(resFinal);
  }

  dividir(f: NgForm){
    if(f.invalid && !this.verAlert){
      this.verAlert = true;
      return;

    }
    this.verAlert = false;
    let res = true;
    this.verResultadoHijo.emit(res);
    if(this.one != null || this.one != 0){
    let  resFinal = this.one / this.second;
    this.resultadoHijo.emit(resFinal);
    }
  }

  limpiar(){
    let res = false;
    this.verResultadoHijo.emit(res);
  }

}
