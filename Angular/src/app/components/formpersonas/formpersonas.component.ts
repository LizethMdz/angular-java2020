import { PersonasService } from './../../services/personas.service';
import { LoggingService } from './../../services/logging.service';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { PersonaModel } from 'src/app/persona.model';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-formpersonas',
  templateUrl: './formpersonas.component.html',
  styleUrls: ['./formpersonas.component.css']
})
export class FormpersonasComponent implements OnInit {
  //COMMENT MISMO CODIGO FUNCIONAL
  //@Output() personaAdd = new EventEmitter<PersonaModel>();

  constructor(private logginService: LoggingService,
              private persnaService: PersonasService,
              private route: Router,
              private router: ActivatedRoute){
                this.persnaService.saludar.subscribe(
                  (indice: number) => alert('El indice: ' + indice)
                )
              }
  //////////////////////////////
  nombre: string;
  edad: number;
  index: number;
  modoEdicion: number;

  /////////////////////////////
  //Referencia local
  // @ViewChild('nombre') nombreInput: ElementRef;
  // @ViewChild('edad') edadInput: ElementRef;

  ngOnInit(): void {
    this.index = this.router.snapshot.params['id'];
    this.modoEdicion = +this.router.snapshot.queryParams['modoEdicion'];
    if (this.modoEdicion != null && this.modoEdicion === 1) {
      console.log(this.modoEdicion);
      let persona: PersonaModel = this.persnaService.encontrarPersona(this.index);
      this.edad = persona.edad;
      this.nombre = persona.nombre;
    }else{
      console.log('No se hizo nada');
    }
  }

  onGuardarPersona(){
    //let persona  = new PersonaModel(this.nombreInput.nativeElement.value, this.edadInput.nativeElement.value);
    let persona  = new PersonaModel(this.nombre, this.edad);
    if (this.modoEdicion != null && this.modoEdicion === 1) {
      this.persnaService.modificarPersona(this.index, persona);
    } else {
      //COMMENT MISMO CODIGO FUNCIONAL
      //this.logginService.enviarMensajesConsola("Enviamos una persona: " + persona.nombre +  ' ' + persona.edad);
      //this.personaAdd.emit(persona);
      this.persnaService.agregarPersona(persona);
    }

    this.route.navigate(['personas']); 
  }

  onEliminarPersona(){
    if (this.index != null) {
      this.persnaService.eliminarPersona(this.index); 
    }
    this.route.navigate(['personas']); 
  }

}
