import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoggingService } from '../services/logging.service';
import { PersonasService } from '../services/personas.service';
import { PersonaModel } from '../persona.model';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-contenedor',
    templateUrl: './contenedor.component.html'
})
export class ContenedorComponent implements OnInit {
    personas : PersonaModel[] = [];

    constructor(private logService: LoggingService,
                private personaService: PersonasService,
                private route: Router){}


    ngOnInit(): void {
        this.logService.enviarMensajesConsola('Listando personas');
        this.personaService.obtenerPersonas().subscribe(
            (data: PersonaModel[]) => {
                this.personas = data;
                this.personaService.setPersonas(data);
            }
        );
    }

    agregar(){
        this.route.navigate(['personas/agregar']);
    }
}
