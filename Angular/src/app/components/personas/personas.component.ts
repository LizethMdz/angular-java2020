import { PersonaModel } from './../../persona.model';
import { Component, OnInit, Input } from '@angular/core';
import { PersonasService } from 'src/app/services/personas.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  @Input() persona:PersonaModel;
  @Input() tam: number;
  @Input() indice: number;
  constructor(private pS: PersonasService) { }

  ngOnInit(): void {
  }
  //NOTE EMITIR LA INFORMACION ENTRE COMPONENETES
  emitirSaludo(){
    this.pS.saludar.emit(this.indice);
  }

}
