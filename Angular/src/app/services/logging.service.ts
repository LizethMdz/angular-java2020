import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  constructor() { }

  enviarMensajesConsola(mensaje: string){
    console.log("Tu mensaje: " + mensaje);
  }
}
