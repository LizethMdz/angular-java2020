import { LoggingService } from './logging.service';
import { Injectable, EventEmitter } from '@angular/core';
import { PersonaModel } from '../persona.model';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  saludar = new EventEmitter<number>();

  constructor(private logService: LoggingService,
              private dataService: DataService) { }

  personas : PersonaModel[] = [];

  setPersonas(personas: PersonaModel[]){
    this.personas = personas;
  }

  agregarPersona(persona: PersonaModel){
    this.logService.enviarMensajesConsola('Agregar persona: ' + persona.nombre);
    if (this.personas == null) {
      this.personas = [];
    }
    this.personas.push(persona);
    this.dataService.guardarPersona(this.personas);
  }

  encontrarPersona(index: number){
    let persona: PersonaModel = this.personas[index];
    return persona;
  }

  modificarPersona(index: number, persona: PersonaModel){
    let persona1= this.personas[index];
    persona1.nombre = persona.nombre;
    persona1.edad = persona.edad;
    this.dataService.modificarPersona(index, persona);
  }

  eliminarPersona(index: number){
    this.personas.splice(index, 1);
    this.dataService.eliminarPersona(index);
    //Regenerar el indice en la BD
    this.regenerarArrPersonas();
  }

  obtenerPersonas(){
    return this.dataService.cargarPersonas();
  }
  
  regenerarArrPersonas(){
    if(this.personas != null){
      this.dataService.guardarPersona(this.personas);
    }
  }


}
