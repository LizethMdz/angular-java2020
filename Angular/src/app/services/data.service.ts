import { LoginService } from './login.service';
import { PersonaModel } from './../persona.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient,
              private loginS: LoginService) { }

  url = 'https://personal-angular-1e27d.firebaseio.com/';

  cargarPersonas(){
    const token = this.loginS.getIdToken();
    return this.http.get(this.url + '/datos.json?auth=' + token);
  }

  //Save Persona
  guardarPersona(personas: PersonaModel[]){
    const token = this.loginS.getIdToken();
    this.http.put( this.url + '/datos.json?auth=' + token , personas)
    .subscribe(
      response => console.log('Resultado guardar personas: ' + response),
      error => console.log('Error al guardar Personas: ' + error)
    );
  }

  modificarPersona(index: number, persona: PersonaModel){
    const token = this.loginS.getIdToken();
    let url = 'https://personal-angular-1e27d.firebaseio.com/datos/' + index + '.json?auth=' + token;
    this.http.put(url , persona).subscribe(
      response => console.log('Resultado de modificar la persona: ' ,response),
      error => console.log('Error al modificar: ' , error)
    );
  }

  eliminarPersona(index: number){
    const token = this.loginS.getIdToken();
    let url = 'https://personal-angular-1e27d.firebaseio.com/datos/' + index + '.json?auth=' + token;
    this.http.delete(url).subscribe(
      response => console.log('Resultado de eliminar la persona: ' ,response),
      error => console.log('Error al eliminar: ' , error)
    );
  }
}
