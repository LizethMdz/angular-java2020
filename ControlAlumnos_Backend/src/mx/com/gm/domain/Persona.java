package mx.com.gm.domain;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@NamedQueries({
	@NamedQuery(name="Persona.allPeople", query = "SELECT p FROM Persona p ORDER BY p.idPersona")
})
public class Persona implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_persona")
	private int idPersona;
	
	private String Nombre;
	
	private String Apellido;
	
	private String Email;
	
	private String Telefono;
	
	

	public Persona() {
	}

	public Persona(int idPersona) {
		this.idPersona = idPersona;
	}

	

	public Persona(int idPersona, String nombre, String apellido, String email, String telefono) {
		this.idPersona = idPersona;
		this.Nombre = nombre;
		this.Apellido = apellido;
		this.Email = email;
		this.Telefono = telefono;
	}

	public int getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getTelefono() {
		return Telefono;
	}

	public void setTelefono(String telefono) {
		Telefono = telefono;
	}

	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", Nombre=" + Nombre + ", Apellido=" + Apellido + ", Email=" + Email
				+ ", Telefono=" + Telefono + "]";
	}

	
	
}
