package mx.com.gm.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.gm.domain.Persona;

@Stateless
public class PersonaDaoImpl implements PersonaDao {

	@PersistenceContext (unitName = "PersonaPU")
	
	//Variable que recibe la unidad de persistencia
	EntityManager em;
	
	@Override
	public List<Persona> encontrarTodasPersonas() {
		// TODO Auto-generated method stub
		return em.createNamedQuery("Persona.allPeople").getResultList();
	}

	@Override
	public Persona encontrarPersona(Persona persona) {
		// TODO Auto-generated method stub
		return em.find(Persona.class, persona.getIdPersona());
	}

	@Override
	public void insertarPersona(Persona persona) {
		// TODO Auto-generated method stub
		em.persist(persona);
		//Regrese un objeto con la llave primaria
		em.flush();
	}

	@Override
	public void actualizarPersona(Persona persona) {
		// TODO Auto-generated method stub
		em.merge(persona);
	}

	@Override
	public void eliminarPersona(Persona persona) {
		// TODO Auto-generated method stub
		em.remove(em.merge(persona));
	}

}
