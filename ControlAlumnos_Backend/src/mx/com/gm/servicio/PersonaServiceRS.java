package mx.com.gm.servicio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import mx.com.gm.data.PersonaDao;
import mx.com.gm.domain.Persona;

@Stateless
@Path("/personas")
public class PersonaServiceRS {
	
	@Inject
	private PersonaDao personaDao;
	
	@GET
	@Produces(value = MediaType.APPLICATION_JSON)
	public List<Persona> listarPersonas(){
		List<Persona> personas= this.personaDao.encontrarTodasPersonas();
		System.out.println("Personas encontradas: " + personas);
		return personas;
	}
	
	@GET
	@Produces( value = MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Persona encontrarPersona(@PathParam("id") int id) {
		Persona persona =  personaDao.encontrarPersona(new Persona(id));
		System.out.println("Persona encontrada: " + persona);
		return persona;
		
	}
	
	@POST
	@Produces(value = MediaType.APPLICATION_JSON)
	@Consumes( value = MediaType.APPLICATION_JSON)
	public Persona agregarPersona(Persona persona) {
			this.personaDao.insertarPersona(persona);
			System.out.println("Persona agregada: " + persona);
			return persona;
	}
	
	
	@PUT
	@Produces(value = MediaType.APPLICATION_JSON)
	@Consumes( value = MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response actualizarPersona(@PathParam("id") int id, Persona personaMod) {
		try {
			Persona persona = this.personaDao.encontrarPersona(new Persona(id));
			if(persona != null) {
				this.personaDao.actualizarPersona(personaMod);
				System.out.println("Persona modificada: " + persona);
				return Response.ok().entity(personaMod).build();
			}else {
				return Response.status(Status.NOT_FOUND).build();
			}
		}catch(Exception e) {
			e.printStackTrace(System.out);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@DELETE
	@Produces(value = MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response eliminarPersona(@PathParam("id") int id) {
		try {
			this.personaDao.eliminarPersona(new Persona(id));
			System.out.println("Persona eliminada con el ID: " + id);
			return Response.ok().build();
		}catch(Exception e) {
			e.printStackTrace(System.out);
			return Response.status(404).build();
		}
	}
	
}
