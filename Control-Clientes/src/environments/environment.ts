// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore: {
    apiKey: "AIzaSyBd-IO2cmoc60nkU5WoN_D0pTGAl88wZCo",
    authDomain: "control-clientes-318f2.firebaseapp.com",
    databaseURL: "https://control-clientes-318f2.firebaseio.com",
    projectId: "control-clientes-318f2",
    storageBucket: "control-clientes-318f2.appspot.com",
    messagingSenderId: "115662030715",
    appId: "1:115662030715:web:b67cd1b18b052f3990dfde",
    measurementId: "G-ZQ972H906R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
