import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Configuracion } from '../model/configuracion';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  configuracionDoc: AngularFirestoreDocument<Configuracion>;
  configuracion: Observable<Configuracion>;
  id = '1';

  constructor(private db: AngularFirestore) { }

  getConfiguration(): Observable<Configuracion>{
    this.configuracionDoc = this.db.collection('Configuracion').doc<Configuracion>(this.id);
    this.configuracion = this.configuracionDoc.valueChanges();
    return this.configuracion;
  }

  setConfiguration(configuracion: Configuracion){
    this.configuracionDoc = this.db.collection('Configuracion').doc<Configuracion>(this.id);
    this.configuracionDoc.update(configuracion);
  }


}
