import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Cliente } from '../model/cliente';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  clientesColeccion: AngularFirestoreCollection<Cliente>;
  clienteDoc: AngularFirestoreDocument<Cliente>;
  clientes: Observable<Cliente[]>;
  cliente: Observable<Cliente>;

  constructor(private db: AngularFirestore) {
    this.clientesColeccion = db.collection<Cliente>('Clientes', ref => ref.orderBy('nombre', 'asc'));
    //NOTE - Comprobacion de extraccion de datos
    //console.log(this.getClientes().subscribe( d => console.log(d)));
    //NOTE: COMPROBACION DE GETUSER()
    //this.getUser('eoczMiYkU59wn0euVlfL').subscribe( d => console.log(d));
  }

  getClientes(): Observable<Cliente[]>{
    //Obtener clientes
    this.clientes = this.clientesColeccion.snapshotChanges().pipe(
      map( cambios => cambios.map(accion => {
          const datos = accion.payload.doc.data() as Cliente;
          datos.id = accion.payload.doc.id;
          return datos;
        })
      )
    );
    return this.clientes;
  }

  agregarClienteFire(cliente: Cliente){
    this.clientesColeccion.add(cliente);
  }
  
  /**Metodo no funcional */
  getCliente(id: string){
    console.log(id);
    //this.clienteDoc = this.db.doc<Cliente>(`clientes/${id}`);
    this.clienteDoc = this.db.collection('Clientes').doc<Cliente>(id);
    this.cliente = this.clienteDoc.snapshotChanges().pipe(
      map( accion => {
        console.log('Accion var', accion );
        if (accion.payload.exists === false) {
          console.log('Regrso null');
          return null;
        }else{
          const datos = accion.payload.data() as Cliente;
          datos.id = accion.payload.id;
          console.log('Regreso informacion');
          return datos;
        }
      })
    );
    return this.cliente;
  }
  /** Metodo Funcional */
  getUser(id: string){
    console.log(id);
    this.clienteDoc = this.db.collection('Clientes').doc<Cliente>(id);
    this.cliente = this.clienteDoc.snapshotChanges().pipe(
      map( accion => {
        if (accion.payload.exists === false) {
          //console.log('Regrso null');
          return null;
        }else{
          const datos = accion.payload.data() as Cliente;
          datos.id = accion.payload.id;
          //console.log('Regreso informacion');
          return datos;
        }
      })
    );
    return this.cliente;
    //return this.db.collection('Clientes').doc(id).snapshotChanges();
  }

  modificarClienteFire(cliente: Cliente){
    this.clienteDoc = this.db.collection('Clientes').doc<Cliente>(cliente.id);
    this.clienteDoc.update(cliente);
  }

  eliminarClienteFire(cliente: Cliente){
    this.clienteDoc = this.db.collection('Clientes').doc<Cliente>(cliente.id);
    this.clienteDoc.delete();
  }


}
