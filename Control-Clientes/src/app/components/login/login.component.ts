import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password: string;

  constructor(private router: Router,
              private flassM: FlashMessagesService,
              private loginS: LoginService) { }

  ngOnInit(): void {
    /**Verifica si se hizo un logeo, de lo contrario simpre redireccionará al Inicio */
    this.loginS.getAuth().subscribe(
      auth => {
        if(auth){
          this.router.navigate(['/']);
        }
      }
    )
  }
  /**Procesa el servicio del login */
  login(){
    this.loginS.login(this.email, this.password)
    .then(res => {
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.flassM.show(error , {
        cssClass: 'alert-danger', timeout:4000
      });
    });
  }

}
