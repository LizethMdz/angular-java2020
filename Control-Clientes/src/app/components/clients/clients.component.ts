import { Cliente } from './../../model/cliente';
import { ClienteService } from './../../services/cliente.service';
import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clientes: Cliente[] = [];
  
  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0
  };

  //Definir un atributo
  @ViewChild('f') fCliente: NgForm;
  @ViewChild("btnClose") btnClose: ElementRef;

  constructor(private clientesS: ClienteService,
              private flassM: FlashMessagesService,
              private renderer: Renderer2) { }

  ngOnInit(): void {
    this.clientesS.getClientes().subscribe(
      clientes => {
        this.clientes = clientes;
        //console.log(this.clientes);
      }
    )
  }

  getSaldoTotal(){
    let suma: number = 0;
    if (this.clientes != null) {
      this.clientes.forEach(element => {
        suma += element.saldo;
      });
      return suma;
    }
  }

  agregarCliente({value, valid} : {value: Cliente, valid: boolean}){
    if (!valid) {
      this.flassM.show('Por favor llena el formulario correctamente', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }else{
      //Agregar el elemento con los metodos del servico
      this.clientesS.agregarClienteFire(value);
      this.fCliente.resetForm();
      this.cerrarModal();
    }
  }

  private cerrarModal(){
    this.renderer.selectRootElement(this.btnClose.nativeElement).click();
  }

}
