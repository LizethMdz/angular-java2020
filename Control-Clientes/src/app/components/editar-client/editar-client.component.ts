import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/app/model/cliente';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-editar-client',
  templateUrl: './editar-client.component.html',
  styleUrls: ['./editar-client.component.css']
})
export class EditarClientComponent implements OnInit {

  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0
  };

  id: string;
  constructor(private clientesS: ClienteService,
              private router: Router,
              private route: ActivatedRoute,
              private flassM: FlashMessagesService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    // this.clientesS.getCliente(this.id).subscribe(cliente => {
    //   this.cliente = cliente;
    //   console.log(cliente);
    // });
    this.clientesS.getUser(this.id).subscribe(cliente => {
      this.cliente = cliente;
      console.log(cliente);
    });
  }

  editarCliente({value, valid} : {value: Cliente, valid: boolean}){
    if (!valid) {
      this.flassM.show('Por favor llena el formulario correctamente', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }else{
      value.id = this.id;
      //Modificarel elemento con los metodos del servico
      this.clientesS.modificarClienteFire(value);
      this.router.navigate(['/']);
    }
  }

  eliminarCliente(){
    if (confirm('¿Seguro que quiere eliminar?')) {
      this.clientesS.eliminarClienteFire(this.cliente);
      this.router.navigate(['/']);
    }
  }

}
