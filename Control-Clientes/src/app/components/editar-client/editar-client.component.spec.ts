import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarClientComponent } from './editar-client.component';

describe('EditarClientComponent', () => {
  let component: EditarClientComponent;
  let fixture: ComponentFixture<EditarClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
