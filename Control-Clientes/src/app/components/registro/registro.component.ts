import { LoginService } from 'src/app/services/login.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  email: string;
  password: string;

  constructor(private router: Router,
              private flassM: FlashMessagesService,
              private loginService: LoginService) { }

  ngOnInit(): void {
     /**Verifica si se hizo un logeo, de lo contrario simpre redireccionará al Inicio */
    this.loginService.getAuth().subscribe(
      auth => {
        if(auth){
          this.router.navigate(['/']);
        }
      }
    )
  }

  registro(){
    this.loginService.signUp(this.email, this.password)
    .then( res => {
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.flassM.show(error, {
        cssClass: 'alert-danger', timeout: 4000
      });
    });
  }

}
