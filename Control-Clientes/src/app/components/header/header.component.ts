import { ConfigurationService } from './../../services/configuration.service';
import { Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Configuracion } from 'src/app/model/configuracion';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  isLoggedIn: boolean;
  loggedInUser: string;
  permitirR: boolean;

  constructor(private loginS: LoginService,
              private router: Router,
              private confService: ConfigurationService) { }

  ngOnInit(): void {
    this.loginS.getAuth().subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
        this.loggedInUser = auth.email;
      }else{
        this.isLoggedIn = false;
      }
    });

    this.confService.getConfiguration().subscribe(
      (conf: Configuracion) => {
        this.permitirR = conf.permitirRegistro;
      }
    )
  }

  logout(){
    this.loginS.logOut();
    this.isLoggedIn = false;
    this.router.navigate(['/login']);
  }

}
