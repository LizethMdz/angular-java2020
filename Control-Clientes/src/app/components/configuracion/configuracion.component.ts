import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Configuracion } from 'src/app/model/configuracion';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {
  
  permitirR = false;
  constructor(private router:Router,
              private confService: ConfigurationService) { }

  ngOnInit(): void {
    this.confService.getConfiguration().subscribe(
      (conf: Configuracion) => {
        this.permitirR = conf.permitirRegistro;
      }
    )
  }

  guardar(){
    let configuracion = { permitirRegistro: this.permitirR };
    this.confService.setConfiguration(configuracion);
    this.router.navigate(['/']);
  }

}
