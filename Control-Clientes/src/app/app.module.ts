import { ConfigurationGuard } from './guards/configuration.guard';
import { LoginService } from './services/login.service';
import { ClienteService } from './services/cliente.service';
import { environment } from './../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, SETTINGS } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { TableroComponent } from './components/tablero/tablero.component';
import { ClientsComponent } from './components/clients/clients.component';
import { EditarClientComponent } from './components/editar-client/editar-client.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { NoFoundComponent } from './components/no-found/no-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthGuard } from './guards/auth.guard';
import { ConfigurationService } from './services/configuration.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TableroComponent,
    ClientsComponent,
    EditarClientComponent,
    LoginComponent,
    RegistroComponent,
    ConfiguracionComponent,
    NoFoundComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firestore),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FlashMessagesModule.forRoot(),
    FormsModule

  ],
  providers: [ClienteService, LoginService, AuthGuard, ConfigurationGuard, ConfigurationService,
              {provide : SETTINGS, useValue: {}}
            ],
  bootstrap: [AppComponent]
})
export class AppModule { }
