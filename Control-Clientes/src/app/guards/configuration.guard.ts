import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ConfigurationService } from '../services/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationGuard implements CanActivate {
  constructor(private router: Router,
              private confService: ConfigurationService){}

  canActivate(): Observable<boolean> {
    return this.confService.getConfiguration().pipe(
      map( conf => {
        if (conf.permitirRegistro) {
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      })
    );
  }
  
}
